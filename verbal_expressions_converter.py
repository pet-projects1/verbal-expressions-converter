from functools import reduce

ones = {
    0: '', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six',
    7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve',
    13: 'thirteen', 14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
    17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}
tens = {
    2: 'twenty', 3: 'thirty', 4: 'forty', 5: 'fifty', 6: 'sixty',
    7: 'seventy', 8: 'eighty', 9: 'ninety'}
order = {
    1: 'thousand', 2: 'million', 3: 'billion', 4: 'trillion', 5: 'quadrillion',
    6: 'quintillion'}

signs = {
    '+': 'plus',
    '-': 'subtract',
    '*': 'multiply',
    '/': 'divide',
    '=': 'equals'}


def number_to_string(number):
    #Adds different behaviors for positive and negative numbers
    number = int(number)

    def __number_to_string(number):
        #Recursive function that converts an integer number to a verbal representation
        if (number < 20):
            return ones[number]
        if (number < 100):
            return ' '.join((tens[number // 10], ones[number % 10]))
        if (number < 1000):
            return separate(number, 100, 'hundred')
        for order_value, order_name in order.items():
            if (number < 1000**(order_value + 1)):
                break
        return separate(number, 1000**order_value, order_name)

    def separate(number, order_value, order_name):
        return ' '.join((
            __number_to_string(number // order_value),
            order_name,
            __number_to_string(number % order_value),
        ))

    if (number < 0):
        return ' '.join(('negative', __number_to_string(-number)))
    if (number == 0):
        return 'zero'
    return __number_to_string(number)


def split_expression(expression):
    #Converts string expression to list on numbers and signs
    def check_exceptions(expression):
        #Numbers that are facing minus become negative
        modification_flag = True
        while (modification_flag):
            modification_flag = False
            for i in range(len(expression) - 1):
                if ((expression[i] == '=') and (expression[i + 1] == '-')):
                    sign = expression.pop(i + 1)
                    expression[i + 1] = ''.join((sign, expression[i + 1]))
                    modification_flag = True
                    break
                if ((i == 0) and (expression[i] == '-')):
                    sign = expression.pop(i)
                    expression[i] = ''.join((sign, expression[i]))
                    modification_flag = True
                    break
                # if ((expression[i] == '=') and (expression[i + 1] == '+')):
                #     sign = expression.pop(i + 1)
                #     modification_flag = True
                #     break
                # if ((i == 0) and (expression[i] == '+')):
                #     sign = expression.pop(i)
                #     modification_flag = True
                #     break
        return modification_flag

    def check_errors(expression):
        if (expression==''): raise ValueError('Input expression cannot be an empty string')
        #Returns an error if the number in the middle contains a space
        for i in range(1, len(expression)):
            if ((expression[i] == ' ') and (
                    expression[i - 1].isdigit()) and (expression[i + 1].isdigit())):
                raise ValueError('The number inside contains a space')

    check_errors(expression)
    expression = expression.replace(" ", "")

    expression = [expression]
    for sign in signs:
        expression = list(map(lambda x: x.split(sign), expression))

        for part in expression:
            if (len(part) != 1):
                for index in range(len(part) - 1, 0, -1):
                    part.insert(index, sign)
        expression = reduce(lambda x, y: x + y, expression)
        # Removes empty strings if they occur
        expression = [x for x in expression if x]
    check_exceptions(expression)
    return expression


def isnumber(data):
    return (((data[0] == '-') and (data[1:].isdigit())) or (data.isdigit()))


def map_string_to_sentence(sequence):
    #main converting function
    #Converts an expression to a verbal representation
    def check_input(sequence):
        #Checks wrong input conditions
        for i in range(len(sequence)):
            if (((i == len(sequence) - 1) and (sequence[i] in signs))
                or ((i == 0) and (sequence[i] in signs) and (sequence[i] != '-') and (isnumber(sequence[i + 1])))
                or ((sequence[i] in signs) and not ((isnumber(sequence[i - 1])) and (isnumber(sequence[i + 1]))))
                    or ((sequence[i] == '=') and (sequence[i + 1] in signs) and (sequence[i + 1] != '-'))):
                raise ValueError('Numbers should surround mathematical signs')
        for i in range(len(sequence)):
            if (isnumber(sequence[i])) and not (((i != 0)and(
                    sequence[i - 1] in signs)) or ((i != len(sequence) - 1)and(sequence[i + 1] in signs))):
                raise ValueError(
                    'Mathematical operations must be beside numbers')

    def map_to_dict(sign):
        if (sign in signs):
            return signs[sign]
        elif (sign.isdigit()) or ((sign[0] == '-') and (sign[1:].isdigit())):
            return number_to_string(sign)
        else:
            raise ValueError(
                'The string contains invalid characters.\nOnly numbers or (+, -, *, /, =) can be used.')

    try:
        sequence = split_expression(sequence)
        check_input(sequence)
        sequence = list(map(map_to_dict, sequence))
        sequence = ' '.join(sequence)
        sequence=' '.join(sequence.split())
    except ValueError:
        return('invalid input')
    return sequence


def map_func_check(data, expected_output):
    #Used for tests using pytest
    output = map_string_to_sentence(data)
    assert output == expected_output, \
        "\nfor: {}\nexpected: {}\ngot: {}\n{}".format(
            data, expected_output, output)


if __name__ == '__main__':
    while True:
        print('Write mathematical expression to convert(\'n\' to exit): ', end='')
        data = input()
        if (data == 'n') or (data == '\'n\''):
            quit()
        print('Resulting sentence:',map_string_to_sentence(data), end='\n\n\n')
