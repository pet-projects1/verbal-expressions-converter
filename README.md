# Verbal expressions converter

This project converts mathematical expressions to their verbal form

## Getting Started
### Prerequisites

For running this project you need pytest module

For installing pytest type in bash
```
$ pip install pytest
```

### Installing

This project does not need additional installations, just copy this repository with the command

```
$ git clone https://gitlab.com/mihaylenko/verbal-expressions-converter
```

### Using

To run a project, open bash in the project directory and folow next steps

```
$ python verbal_expressions_converter.py
```

Then type your expression

```
Write mathematical expression to convert('n' to exit): 1+2=3
```

You will get 
```
Resulting sentence: one plus two equals three
```
or `invalid input` if you typed wrong expression

## Running the tests

To run the automated tests simply open bash in this project directory and type
```
$ pytest vec_tests.py
```

## Author

* **Bohdan Mykhailenko** - [Mihaylenko](https://gitlab.com/mihaylenko)