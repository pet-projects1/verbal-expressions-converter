from verbal_expressions_converter import map_func_check


def test_simple():
    map_func_check('3+7=10', 'three plus seven equals ten')


def test_all_operations():
    map_func_check(
        '3-6/2+8*12=-96',
        'three subtract six divide two plus eight multiply twelve equals negative ninety six')


def test_work_with_negative_numbers():
    map_func_check('-3+5=7', 'negative three plus five equals seven')
    map_func_check('3-5=-2', 'three subtract five equals negative two')


def test_with_less_than_nineteen():
    map_func_check(
        '12+19+15+17=63',
        'twelve plus nineteen plus fifteen plus seventeen equals sixty three')


def test_with_zeros():
    map_func_check('103+10003', 'one hundred three plus ten thousand three')


def test_invalid_number():
    map_func_check('a+1', 'invalid input')
    map_func_check('12+4a1*3', 'invalid input')


def test_invalid_sign():
    map_func_check('12%3=0', 'invalid input')
    map_func_check('12+(5-3)=14', 'invalid input')


def test_space_in_number():
    map_func_check('45 3+7=460', 'invalid input')


def test_work_with_large_numbers():
    map_func_check(
        '999999999999999999999+1429530472405728',
        'nine hundred ninety nine quintillion nine hundred ninety ' +
        'nine quadrillion nine hundred ninety nine trillion nine hundred ' +
        'ninety nine billion nine hundred ninety nine million nine hundred ' +
        'ninety nine thousand nine hundred ninety nine plus one quadrillion ' +
        'four hundred twenty nine trillion five hundred thirty billion four ' +
        'hundred seventy two million four hundred five thousand seven hundred ' +
        'twenty eight')


def test_non_expression_input():
    map_func_check('1', 'invalid input')
    map_func_check('+', 'invalid input')
    map_func_check('3++5', 'invalid input')
    map_func_check('1+6*', 'invalid input')
